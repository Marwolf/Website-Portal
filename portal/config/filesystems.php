<?php

return [

    'disks' => [
        'sqlite' => [
            'driver' => 'local',
            'root' => base_path('sqlite'),
        ],
    ],

];
